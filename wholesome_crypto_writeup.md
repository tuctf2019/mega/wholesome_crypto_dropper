# Wholesome Crypto -- TUCTF2019

Recognizing that there no Pokemon are ever paired with a different Pokemon is key to solve the challenge. Knowing this, assign each pair to a letter, accounting for repeat pairs. This will give you: `A BCDE FCGHEI JCKLIEML`. Running this through [quipqiup](https://quipqiup.com/) gives "I Love Pocket Monsters" as the fourth result which is the answer.

Flag: `TUCTF{M4NY_ST3PS_3QU4LS_B1G_R3W4RD}`
