#!/usr/bin/env python

import socket, sys

from base64 import b64encode

from _thread import *

SERVER_MODE = True

HOST = ''
PORT = 8888

TIMEOUT = 30

with open('cryptogram.jpg', 'rb') as f:
    DATA = b64encode(f.read()).decode('utf-8')

if SERVER_MODE:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')

    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        sys.stdout.write(f'Bind failed. {msg}\n\n')
        sys.stdout.flush()
        sys.exit()

    print('Socket bind complete')

    s.listen(10)
    print('Socket now listening')

def clienthread(conn):
    try:
        conn.sendall(DATA.encode('utf-8'))

        conn.close()
    except (BrokenPipeError, ConnectionResetError, socket.timeout):
        pass

if SERVER_MODE:
    while True:
        try:
            conn, addr = s.accept()
            print('Connected with ' + addr[0] + ': ' + str(addr[1]))
            conn.settimeout(TIMEOUT)

            start_new_thread(clienthread, (conn,))
        except KeyboardInterrupt:
            break
    sys.stdout.write('Shutting down\n')
    sys.stdout.flush()
else:
    sys.stdout.write(DATA)
    sys.stdout.flush()
